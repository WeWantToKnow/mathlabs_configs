Quick And Dirty no need for a micro service data store for our configurations.

For each configuration add a folder named $configID containing three files:

* $configID/metadata.json   <- 2 fields: "description" and "tool_name"
* $configID/config.json     <- the config
* $configID/image.png       <- last image
